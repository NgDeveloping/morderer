package de.Ng.Murder.FakePlayer;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.mojang.authlib.GameProfile;

import de.Ng.Murder.Util.MathUtil;
import net.minecraft.server.v1_12_R1.BlockPosition;
import net.minecraft.server.v1_12_R1.ChatComponentText;
import net.minecraft.server.v1_12_R1.DataWatcher;
import net.minecraft.server.v1_12_R1.DataWatcherObject;
import net.minecraft.server.v1_12_R1.Entity;
import net.minecraft.server.v1_12_R1.EntityHuman;
import net.minecraft.server.v1_12_R1.EntityLiving;
import net.minecraft.server.v1_12_R1.EnumGamemode;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayOutBed;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityHeadRotation;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityTeleport;
import net.minecraft.server.v1_12_R1.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_12_R1.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_12_R1.PacketPlayOutPlayerInfo.EnumPlayerInfoAction;

@SuppressWarnings("rawtypes")
public class FakePlayer extends Reflection {
	
	public static List<FakePlayer> existFakePlayers = new LinkedList<>();
	
	public static World world = Bukkit.getWorld("world");
	public static Location bed0 = new Location(world, 0, 0, 1);
	public static Location bed1 = new Location(world, -1, 0, 0);
	public static Location bed2 = new Location(world, 0, 0, -1);
	public static Location bed3 = new Location(world, 1, 0, 0);
	
	public static void spawnAllFakePlayers(Player player, boolean initPlayer) {
		if(initPlayer)
			initPlayer(player);
		
		existFakePlayers.forEach(fakePlayer -> {
			sendPacket(player, fakePlayer.spawn(false));
			if(fakePlayer.isInBed())
				fakePlayer.setSleeping(player, false);
		});
	}
	
	public static void destroyAllFakePlayers() {
		existFakePlayers.forEach(fakePlayer -> fakePlayer.destroy());
	}
	
	public static void initAllPlayers() {
		Bukkit.getOnlinePlayers().forEach(player -> {
			((CraftPlayer)player).sendBlockChange(bed0, Material.BED_BLOCK, (byte)0);
			((CraftPlayer)player).sendBlockChange(bed1, Material.BED_BLOCK, (byte)1);
			((CraftPlayer)player).sendBlockChange(bed2, Material.BED_BLOCK, (byte)2);
			((CraftPlayer)player).sendBlockChange(bed3, Material.BED_BLOCK, (byte)3);
		});
	}
	
	public static void initPlayer(Player player) {
		((CraftPlayer)player).sendBlockChange(bed0, Material.BED_BLOCK, (byte)0);
		((CraftPlayer)player).sendBlockChange(bed1, Material.BED_BLOCK, (byte)1);
		((CraftPlayer)player).sendBlockChange(bed2, Material.BED_BLOCK, (byte)2);
		((CraftPlayer)player).sendBlockChange(bed3, Material.BED_BLOCK, (byte)3);
	}
	
	public static void initPlayer(List<Player> players) {
		players.forEach(player -> {
			((CraftPlayer)player).sendBlockChange(bed0, Material.BED_BLOCK, (byte)0);
			((CraftPlayer)player).sendBlockChange(bed1, Material.BED_BLOCK, (byte)1);
			((CraftPlayer)player).sendBlockChange(bed2, Material.BED_BLOCK, (byte)2);
			((CraftPlayer)player).sendBlockChange(bed3, Material.BED_BLOCK, (byte)3);
		});
	}
	
	public static List<Packet> addToTabList(List<Player> players) {
		List<Packet> list = new LinkedList<>();
		players.forEach(player -> list.add(addToTabList(player)));
		return list;
	}
	
	public static List<Packet> removeToTabList(List<Player> players) {
		List<Packet> list = new LinkedList<>();
		players.forEach(player -> list.add(removeFromTabList(player)));
		return list;
	}
	
	public static Packet addToTabList(Player player) {
		PacketPlayOutPlayerInfo packet = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER);
		set(packet, "b", Arrays.asList(packet.new PlayerInfoData(((CraftPlayer)player).getProfile(), 0, EnumGamemode.NOT_SET, new ChatComponentText(player.getDisplayName()))));
		return packet;
	}
	
	public static Packet removeFromTabList(Player player) {
		PacketPlayOutPlayerInfo packet = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER);
		set(packet, "b", Arrays.asList(packet.new PlayerInfoData(((CraftPlayer)player).getProfile(), 0, null, null)));
		return packet;
	}
	
	public static void sendPacket(Packet packet) {
		Bukkit.getOnlinePlayers().forEach(player -> ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet));
	}

	public static void sendPacket(Player player, Packet packet) {
		((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
	}
	
	
	public static void sendPacket(List<Player> players, Packet packet) {
		players.forEach(player -> ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet));
	}
	
	private Player player;
	
	private int entityID;
	private Location location;
	private GameProfile gameProfile;
	private DataWatcher dataWatcher;
	private boolean isInBed;
	private Location bedRotation;
	
	@SuppressWarnings("unchecked")
	public FakePlayer(Player player) {
		entityID = (int) get(Entity.class, null, "entityCount");
		set(Entity.class, null, "entityCount", entityID + 1);
		
		this.player = player;
		
		this.location = player.getLocation();
		this.gameProfile = new GameProfile(player.getUniqueId(), player.getName());
		this.dataWatcher = new DataWatcher(null);
		
		byte status = 0;

		changeMask(status, 0, false);//ON FIRE
		changeMask(status, 1, false);//CROUCHED
		changeMask(status, 2, false);//SPRINTING
		changeMask(status, 3, false);//UNUSED
		changeMask(status, 4, true);//INVISIBLE
		changeMask(status, 5, true);//GLOWING EFFECT
		changeMask(status, 6, false);//FLYING WITH ELYTRA
		
		this.dataWatcher.register((DataWatcherObject<Byte>) get(Entity.class, null, "Z"), status);
		this.dataWatcher.register(EntityLiving.HEALTH, 20F);
		
		DataWatcherObject<Byte> skin = (DataWatcherObject<Byte>) get(EntityHuman.class, null, "br");
		this.dataWatcher.register(skin, (byte) ((CraftPlayer)player).getHandle().getDataWatcher().get(skin));
		
		existFakePlayers.add(this);
	}
	
	private byte changeMask(byte bitMask, int bit, boolean state) {
		return state ? (bitMask |= 1 << bit) : (bitMask &= ~(1 << bit));
	}
	
	public Packet spawn(boolean spawn) {
		PacketPlayOutNamedEntitySpawn packet = new PacketPlayOutNamedEntitySpawn();
		
		set(packet, "a", entityID);
		set(packet, "b", gameProfile.getId());
		set(packet, "c", location.getX());
		set(packet, "d", location.getY());
		set(packet, "e", location.getZ());
		set(packet, "f", fixRotation(location.getYaw()));
		set(packet, "g", fixRotation(location.getPitch()));
		set(packet, "h", this.dataWatcher);
		
		if(spawn)
			sendPacket(packet);
		return packet;
	}
	
	public Packet lookTo(LivingEntity entity) {
		double diffX = entity.getLocation().getX() - this.location.getX();
		double diffY = entity.getEyeLocation().getY() - this.location.getY() + 1.6;
		double diffZ = entity.getLocation().getZ() - this.location.getZ();
		double hypoXZ = Math.sqrt(diffX * diffX + diffZ * diffZ);
		location.setYaw((float) (Math.atan2(diffZ, diffX) * 180D / Math.PI) - 90F);
		location.setPitch((float) -(Math.atan2(diffY, hypoXZ) * 180D / Math.PI));
		return teleport(location);
	}
	
	public Packet lookTo(Location location) {
		double diffX = location.getX() - this.location.getX();
		double diffY = location.getY() - this.location.getY();
		double diffZ = location.getZ() - this.location.getZ();
		double hypoXZ = Math.sqrt(diffX * diffX + diffZ * diffZ);
		location.setYaw((float) (Math.atan2(diffZ, diffX) * 180D / Math.PI) - 90F);
		location.setPitch((float) -(Math.atan2(diffY, hypoXZ) * 180D / Math.PI));
		return teleport(location);
	}
	
	public Packet setHeadRotation(float yaw) {
		PacketPlayOutEntityHeadRotation packet = new PacketPlayOutEntityHeadRotation();
		set(packet, "a", entityID);
		set(packet, "b", toAngle(yaw));
		return packet;
	}
	
	public void setSleeping(Byte bedRotation) {
		isInBed = true;
		
		Location bed = bedRotation == 0 ? bed0 : bedRotation == 1 ? bed1 : bedRotation == 2 ? bed2 : bed3;
		
		this.bedRotation = bed;
		
		PacketPlayOutBed packet = new PacketPlayOutBed();
		set(packet, "a", entityID);
		set(packet, "b", new BlockPosition(bed.getBlockX(), bed.getBlockY(), bed.getBlockZ()));
		sendPacket(packet);
		
		location.setYaw(MathUtil.random(-40, 40));
		location.setPitch(MathUtil.random(320, 320));
		
		location.setY(player.getLocation().getY() + .2);
		sendPacket(teleport(location.clone().add(bed)));
	}
	
	public void setSleeping(Player player, boolean initPlayer) {
		isInBed = true;
		
		if(initPlayer)
			initPlayer(player);
		
		PacketPlayOutBed packet = new PacketPlayOutBed();
		set(packet, "a", entityID);
		set(packet, "b", new BlockPosition(bedRotation.getBlockX(), 0, bedRotation.getBlockZ()));
		sendPacket(player, packet);
		
		Vector vector = player.getLocation().getDirection().multiply(2);
		sendPacket(player, teleport(location.clone().add(vector)));
	}
	
	public Packet teleport(Location location) {
		return teleport(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
	}
	
	public Packet teleport(double x, double y, double z, float yaw, float pitch) {
		this.location.setX(x);
		this.location.setY(y);
		this.location.setZ(z);
		this.location.setYaw(yaw);
		this.location.setPitch(pitch);
		
		PacketPlayOutEntityTeleport packet = new PacketPlayOutEntityTeleport();
		set(packet, "a", entityID);
		set(packet, "b", x);
		set(packet, "c", y);
		set(packet, "d", z);
		set(packet, "e", fixRotation(yaw));
		set(packet, "f", fixRotation(pitch));
		set(packet, "g", true);
		
		return packet;
	}
	
	public Packet addToTabList() {
		PacketPlayOutPlayerInfo packet = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER);
		set(packet, "b", Arrays.asList(packet.new PlayerInfoData(gameProfile, 0, EnumGamemode.NOT_SET, new ChatComponentText(player.getDisplayName()))));
		return packet;
	}
	
	public Packet removeFromTabList() {
		PacketPlayOutPlayerInfo packet = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER);
		set(packet, "b", Arrays.asList(packet.new PlayerInfoData(gameProfile, 0, null, null)));
		return packet;
	}
	
	public Packet destroy() {
		PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(entityID);
		return packet;
	}
	
	public byte fixRotation(float yawPitch) {
		return (byte) ((int) (yawPitch * 256 / 360));
	}
	
	public byte toAngle(float value) {
		return (byte) ((int) (value * 256F / 360F));
	}
	
	public DataWatcher getDataWatcher() {
		return dataWatcher;
	}
	
	public GameProfile getGameProfile() {
		return gameProfile;
	}
	
	public Location getLocation() {
		return location;
	}
	
	public boolean isInBed() {
		return isInBed;
	}
	
	public int getEntityID() {
		return entityID;
	}
	
	public Player getPlayer() {
		return player;
	}
}