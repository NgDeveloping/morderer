package de.Ng.Murder.Listener;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;

public class PlayerSwapHandItemsEvent_Listener implements Listener {
	
	@EventHandler
	public void onPlayerSwapHand(PlayerSwapHandItemsEvent event) {
		if(event.getMainHandItem().getType() == Material.GOLD_INGOT || event.getOffHandItem().getType() == Material.GOLD_INGOT ||
				event.getMainHandItem().getType() == Material.BARRIER || event.getOffHandItem().getType() == Material.BARRIER)
			event.setCancelled(true);
	}
}