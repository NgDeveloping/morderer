package de.Ng.Murder.Listener;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

import de.Ng.Murder.Murder;
import de.Ng.Murder.Util.ParticleEffect;

public class EntityDamageByEntityEvent_Listener implements Listener {
	
	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		event.setCancelled(false);
		if(event.getCause() == DamageCause.ENTITY_ATTACK) {
			event.setCancelled(true);
			return;
		}
		if(event.getCause() == DamageCause.PROJECTILE) {
			if(event.getDamager() instanceof Arrow) {
				if(event.getEntity() instanceof ArmorStand) {
					event.getDamager().remove();
					
					if(!Murder.config.getBoolean("StopSwordByArrowHit"))
						return;
					
					ArmorStand armorStand = (ArmorStand)event.getEntity();
					
					if(!armorStand.hasMetadata("FINISH")) {
						armorStand.setMetadata("DOWN", new FixedMetadataValue(Murder.plugin, true));
						armorStand.teleport(armorStand.getLocation().setDirection(new Vector(0, -1, 0)));
						armorStand.setRightArmPose(new EulerAngle(6.1, 1.5, 4.7D));
					}
					
					Location location = event.getEntity().getLocation().add(0, 0.5, 0);
					
					location.getWorld().playSound(location, Sound.BLOCK_SLIME_HIT, 45F,20F);
					
					ParticleEffect.CRIT.display(.2F, .2F, .2F, 0.1F, 5, location, 120);
					return;
				}
				Player shooter = (Player)((Arrow)event.getDamager()).getShooter();
				Player target = (Player)event.getEntity();
				
				if(target == shooter) {
					target.setGameMode(GameMode.SPECTATOR);
					target.sendMessage(Murder.prefix + "Du hast selbstmord begangen�8.");
					return;
				}
				
				shooter.setGameMode(GameMode.SPECTATOR);
				target.setGameMode(GameMode.SPECTATOR);
				
				target.sendMessage(Murder.prefix + "Du wurdest umgebracht�8.");
				shooter.sendMessage(Murder.prefix + "Du hast " + target.getDisplayName() + " umgebracht�8.");
				
				
				return;
			}
		}
	}
	
	public static void playerDead(Player dead, Player killer) {
		
	}
}