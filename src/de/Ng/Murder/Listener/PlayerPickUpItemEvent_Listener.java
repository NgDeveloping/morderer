package de.Ng.Murder.Listener;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

import de.Ng.Murder.Util.ItemFactory;

public class PlayerPickUpItemEvent_Listener implements Listener {
	
	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onPlayerPickUpItem(PlayerPickupItemEvent event) {
		if(event.isCancelled())
			return;
		
		Player player = event.getPlayer();
		
		if(event.getItem().getItemStack().getType() == Material.GOLD_INGOT) {
			event.setCancelled(true);
			
			int ammount = event.getItem().getItemStack().getAmount();
			
			event.getItem().remove();
			
			ItemStack currentStack = player.getInventory().getItem(8);
			
			if(currentStack != null && currentStack.getType() != Material.BARRIER)
				currentStack.setAmount(currentStack.getAmount() + ammount);
			else
				currentStack = new ItemFactory(Material.GOLD_INGOT, 1, "�6Gold", (byte)0).build();
			
			if(currentStack.getAmount() >= 10) {
				currentStack.setAmount(currentStack.getAmount() - 10);
				if(currentStack.getAmount() == 0)
					currentStack = new ItemFactory(Material.BARRIER, 1, "�cDu hast kein �6Gold", (byte)0).build();
				
				if(!player.getInventory().contains(Material.BOW))
					player.getInventory().addItem(new ItemStack(Material.BOW));
				player.getInventory().addItem(new ItemStack(Material.ARROW));
			}
			player.getInventory().setItem(8, currentStack);
			
			player.playSound(player.getLocation(), Sound.ENTITY_ITEM_PICKUP, 1F, 1F);
		}
	}
	
}