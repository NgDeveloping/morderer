package de.Ng.Murder.Listener;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryClickEvent_Listener implements Listener {
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onInventoryClick(InventoryClickEvent event) {
		if(event.getCurrentItem() == null)
			return;
		if(event.getCurrentItem().getType() == Material.GOLD_INGOT || event.getCurrentItem().getType() == Material.BARRIER || (event.getCurrentItem().getType() == Material.IRON_SWORD && event.getCurrentItem().getDurability() != 0 && event.getCurrentItem().getDurability() != -1))
			event.setCancelled(true);
	}
}