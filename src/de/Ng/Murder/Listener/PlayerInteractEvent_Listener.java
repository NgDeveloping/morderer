package de.Ng.Murder.Listener;

import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

import de.Ng.Murder.Murder;
import de.Ng.Murder.FakePlayer.FakePlayer;
import de.Ng.Murder.Util.ActionBar;
import de.Ng.Murder.Util.ParticleEffect;
import net.minecraft.server.v1_12_R1.AxisAlignedBB;
import net.minecraft.server.v1_12_R1.MathHelper;

public class PlayerInteractEvent_Listener implements Listener {
	
	public Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onInteract(PlayerInteractEvent event) {
		if(event.getItem() == null)
			return;
		
		if(event.getClickedBlock() != null && event.getItem().getType() == Material.STICK) {
			event.getPlayer().removeMetadata("murder.swordLaunched", Murder.plugin);
			Bukkit.broadcastMessage("BlockInfo: " + event.getClickedBlock().getType() + 
					"\nData: " + event.getClickedBlock().getData() + 
					"\nName: " + event.getClickedBlock().getType().getData().getName());
			return;
		}
		if(event.getClickedBlock() != null && event.getItem().getType() == Material.BLAZE_ROD) {
			Bukkit.broadcastMessage("Spawn");
			FakePlayer player = new FakePlayer(event.getPlayer());
			player.spawn(true);
			player.setSleeping((byte)0);
			
			Team team = board.getTeam("TEST") == null ? board.registerNewTeam("TEST") : board.getTeam("TEST");
			
			team.setNameTagVisibility(NameTagVisibility.HIDE_FOR_OWN_TEAM);
			team.setPrefix("§4TEST§3: ");
			team.addPlayer(event.getPlayer());
			
			Bukkit.getOnlinePlayers().forEach(p -> p.setScoreboard(board));
			return;
		}
		
		if(/* event.getPlayer().hasMetadata("murder.murder") &&*/ !event.getPlayer().hasMetadata("murder.swordLaunched")  && (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) &&
				event.getItem().getType() == Material.IRON_SWORD) {
			event.setCancelled(true);
			
			Player player = event.getPlayer();
			
			player.setMetadata("murder.swordLaunched", new FixedMetadataValue(Murder.plugin, true));
			
			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 25, 5));
			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 25, 5));
			
			new BukkitRunnable() {
				
				ArmorStand armorStand;
				
				double speed = .25;
				int couldown = 100;
				double charging = 0;
				int length = 0;
				Location location;
				
				boolean finish = false;
				
				@Override
				public void run() {
					if(charging != 20) {
						int count = (int) charging / 2;
						
						String state = "";
						
						for(int i = 0; i < 10; i++)
							if(i <= count)
								state += "§a■";
							else
								state += "§c■";
						
						ActionBar.sendActionBar(player, "§8§l[" + state + "§8§l] §c§l" + (String.valueOf((double) (1D / 20D * charging))).substring(0, 3) + " Sekunden");
						player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1F, (float) (1D / 20D * charging));
						
						charging++;
						
						if(charging == 20) {
							location = player.getLocation().add(0, .8, 0);
							Vector vec = location.getDirection();
							location.add(.1 * -vec.getZ(), 0, .1 * vec.getX());
							
							armorStand = spawnArmorStand(location);
						}
						return;
					}
					
					if(couldown != -1) {
						if(couldown % 20 == 0)
							ActionBar.sendActionBar(player, "§cDu musst noch §4" + couldown / 20 + " §cSekunden warten");
						couldown--;
						
						player.getInventory().forEach(item -> {
							if(item != null && item.getType() == Material.IRON_SWORD)
								item.setDurability((short) (250 / 100 * couldown));
						});
						
						if(couldown == 0) {
							ActionBar.sendActionBar(player, "§cDu musst noch §40 §cSekunden warten");
							player.removeMetadata("murder.swordLaunched", Murder.plugin);
						}
					}
					
					if(finish) {
						if(couldown == 0)
							cancel();
						return;
					}
					
					if(armorStand.isDead() || location.getBlockY() < 0 || location.getBlockY() > 250 || length > 400) {
						finish = true;
						armorStand.remove();
						return;
					}
					
					for(int i = 0; i < 4; i++) {
						length++;
						
						Vector armorStandVector = armorStand.getLocation().getDirection();
						Location armorStandLocation = armorStand.getLocation().add(.1 * armorStandVector.getZ(), 0, .1 * -armorStandVector.getX()), armorStandLocationNext = armorStandLocation.clone().add(0, .65, 0).add(armorStandLocation.getDirection().multiply(speed));
						
						Vector add = armorStand.getLocation().getDirection().multiply(isUnderWater(armorStandLocationNext) ? speed / 4 : speed);
						
						if (isInBlock(armorStandLocationNext, add)) {
							finish = true;
							armorStand.setMetadata("FINISH", new FixedMetadataValue(Murder.plugin, true));
							armorStand.setGravity(false);
							add = armorStand.getLocation().getDirection().multiply(.2);
							armorStand.teleport(armorStand.getLocation().add(add.getX(), add.getY(), add.getZ()));
							return;
						}

						if (armorStand.hasMetadata("DOWN"))
							speed += .01;
						
						armorStand.teleport(armorStand.getLocation().add(add.getX(), add.getY(), add.getZ()));
						
						Collection<Entity> entitys = armorStandLocation.getWorld()
								.getNearbyEntities(armorStand.getLocation().add(0, .65, 0), .2, .2, .2);

						for (Entity entity : entitys) {
							if (entity instanceof Player) {
								Player target = (Player) entity;
								if (target == player || target.getGameMode() == GameMode.SPECTATOR)
									continue;

								target.sendMessage(Murder.prefix + "Du wurdest erschladen§8.");
								target.setGameMode(GameMode.SPECTATOR);

								armorStand.remove();
								finish = true;
								return;
							} else if (entity instanceof Arrow) {
								entity.remove();
								armorStand.remove();
								ParticleEffect.CRIT.display(0, 0, 0, 0.1F, 5, armorStandLocation, 120);
								finish = true;
								return;
							}
						}
					}
				}
			}.runTaskTimer(Murder.plugin, 0, 0);
		}
	}
	
	public ArmorStand spawnArmorStand(Location location) {
		ArmorStand armorStand = (ArmorStand) location.getWorld().spawnEntity(location, EntityType.ARMOR_STAND);
		armorStand.setVisible(false);
		armorStand.setGravity(false);
		armorStand.setSmall(true);
		
		armorStand.setItemInHand(new ItemStack(Material.IRON_SWORD));
		armorStand.setRightArmPose(new EulerAngle(6.1, location.getPitch() / 360 * 6, 4.7D));
		return armorStand;
		
	}
	
	private AxisAlignedBB getBoundingBox(Location location) {
		return new AxisAlignedBB(location.getX() - .05D, location.getY() - .05D, location.getZ() - .05D, location.getX() + .05D, location.getY() + .05D, location.getZ() + .05D);
	}
	
	public boolean isInBlock(Location location, Vector change) {
		double x2 = MathHelper.a(change.getX(), -8, 8);
		double y2 = MathHelper.a(change.getY(), -8, 8);
		double z2 = MathHelper.a(change.getZ(), -8, 8);
		
		AxisAlignedBB swordBox = getBoundingBox(location);
		for(AxisAlignedBB box : ((CraftWorld) location.getWorld()).getHandle().getCubes(null, swordBox.b(x2, y2, z2)))
			if(x2 != box.a(swordBox, x2) || y2 != box.b(swordBox, y2) || z2 != box.c(swordBox, z2))
				return location.getWorld().getBlockAt((int) Math.floor(box.a), (int) Math.floor(box.b), (int) Math.floor(box.c)).getType() != Material.BARRIER;
		return false;
	}
	
	public boolean isUnderWater(Location location) {
		return location.getBlock().isLiquid();
	}
}