package de.Ng.Murder;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import de.Ng.Murder.Command.CMD_Murder;
import de.Ng.Murder.Listener.BlockBreakEvent_Listener;
import de.Ng.Murder.Listener.BlockPlaceEvent_Listener;
import de.Ng.Murder.Listener.EntityDamageByEntityEvent_Listener;
import de.Ng.Murder.Listener.EntityDamageEvent_Listener;
import de.Ng.Murder.Listener.InventoryClickEvent_Listener;
import de.Ng.Murder.Listener.PlayerDropItemEvent_Listener;
import de.Ng.Murder.Listener.PlayerInteractEvent_Listener;
import de.Ng.Murder.Listener.PlayerPickUpItemEvent_Listener;
import de.Ng.Murder.Listener.PlayerSwapHandItemsEvent_Listener;
import de.Ng.Murder.Spawner.Spawner;

public class Murder extends JavaPlugin {
	
	public static Plugin plugin;
	
	public static Spawner spawner;
	
	public static String prefix = "�7[�4Murder�7] �3";
	
	public static Random random = new Random();
	
	public static FileConfiguration config;
	
	public void onEnable() {
		plugin = this;
		
		registerCommands();
		registerListener();
		
		spawner = new Spawner();
		spawner.loadSpawner();
		
		config = YamlConfiguration.loadConfiguration(new File("./plugins/Murder/Basic.yml"));
		config.options().copyDefaults(true);
		
		config.addDefault("StopSwordByArrowHit", true);
		config.addDefault("PlayerPickUpFlippedSword", true);
		config.addDefault("MaxPlayers", 16);
		config.addDefault("MaxMurder", 1);
		config.addDefault("MaxDetectiv", 1);
		
		List<String> calculate = new LinkedList<String>();
		
		calculate.add("1");
		calculate.add("2");
		
		config.addDefault("Calculate.4", calculate);
		
		try {
			config.save(new File("./plugins/Murder/Basic.yml"));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		
		for(int i = 0; i <= config.getInt("MaxPlayers"); i++) {
			List<String> list = config.getStringList("Calculate." + i);
			for(String l : list)
				Bukkit.broadcastMessage("Hey " + i + " - " + l);
		}
	}
	
	public void onDisable() {
	}
	
	public void registerListener() {
		plugin.getServer().getPluginManager().registerEvents(new PlayerPickUpItemEvent_Listener(), this);
		plugin.getServer().getPluginManager().registerEvents(new EntityDamageByEntityEvent_Listener(), this);
		plugin.getServer().getPluginManager().registerEvents(new InventoryClickEvent_Listener(), this);
		plugin.getServer().getPluginManager().registerEvents(new PlayerSwapHandItemsEvent_Listener(), this);
		plugin.getServer().getPluginManager().registerEvents(new PlayerDropItemEvent_Listener(), this);
		plugin.getServer().getPluginManager().registerEvents(new EntityDamageEvent_Listener(), this);
		plugin.getServer().getPluginManager().registerEvents(new BlockBreakEvent_Listener(), this);
		plugin.getServer().getPluginManager().registerEvents(new BlockPlaceEvent_Listener(), this);
		plugin.getServer().getPluginManager().registerEvents(new PlayerInteractEvent_Listener(), this);
	}
	
	public void registerCommands() {
		getCommand("murder").setExecutor(new CMD_Murder());
	}
}