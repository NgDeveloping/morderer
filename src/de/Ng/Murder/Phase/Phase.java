package de.Ng.Murder.Phase;

import org.bukkit.Bukkit;

import de.Ng.Murder.Event.PhaseChangeEvent;

public abstract class Phase {
	
	public abstract void onTick();
	public abstract void onBegin();
	
	public void nextPhase(PhaseType type) {
		
		Bukkit.getPluginManager().callEvent(new PhaseChangeEvent(type));
	}
}