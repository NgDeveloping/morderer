package de.Ng.Murder.Team;

import org.bukkit.scoreboard.Scoreboard;

public class Team {
	
	private Scoreboard scoreBoard;
	private org.bukkit.scoreboard.Team _team;
	
	private String teamName;
	
	public Team(Scoreboard scoreboard, String teamName) {
		this.scoreBoard = scoreboard;
		this.teamName = teamName;
	}
	
	public boolean isTeamExist() {
		return scoreBoard.getTeam(teamName) != null;
	}
	
	public Team createTeam() {
		if(isTeamExist())
			return this;
		
		_team = scoreBoard.registerNewTeam(teamName);
		
		return this;
	}
	
	public Scoreboard getScoreboard() {
		return scoreBoard;
	}
	
	public org.bukkit.scoreboard.Team getTeam() {
		return _team;
	}
	
	public String getTeamName() {
		return teamName;
	}
}