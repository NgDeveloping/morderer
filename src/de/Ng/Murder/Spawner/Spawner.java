package de.Ng.Murder.Spawner;

import java.io.File;
import java.io.FileWriter;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import de.Ng.Murder.Murder;
import de.Ng.Murder.Util.MathUtil;

public class Spawner {

	private File file = new File("./world/locations.murder");
	
	private List<Location> locations = new LinkedList<>();
	
	private ItemStack item = new ItemStack(Material.GOLD_INGOT);
	
	public BukkitRunnable runnable;
	
	public void loadSpawner() {
		try {
			if(!file.exists())
				return;
			
			Scanner scanner = new Scanner(file);
			
			World world = Bukkit.getWorld("world");
			
			while(scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if(line.isEmpty())
					continue;
				String[] args = line.split("#");
				locations.add(new Location(world, Double.parseDouble(args[0]), Double.parseDouble(args[1]), Double.parseDouble(args[2])));
			}
			scanner.close();
			
			Collections.shuffle(locations);
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void saveLocation(Location location) {
		try {
			if(!file.exists())
				file.createNewFile();
			
			location.setX(location.getBlockX() + .5);
			location.setY(location.getBlockY() + .2);
			location.setZ(location.getBlockZ() + .5);
			locations.add(location);
			
			FileWriter writer = new FileWriter(file);
			for(Location loc : locations)
				writer.write(MessageFormat.format("{0}#{1}#{2}\n", loc.getX(), loc.getY(), loc.getZ()));
			writer.close();
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void spawn() {
		runnable = new BukkitRunnable() {
			
			private int CurrentLocation = 0;
			
			@Override
			public void run() {
				if(locations.size() == 0)
					return;
				if(locations.size() == CurrentLocation) {
					CurrentLocation = 0;
					Collections.shuffle(locations);
				}
				
				Location location = locations.get(CurrentLocation);
				CurrentLocation++;
				
				location = checkArea(location, CurrentLocation, 0);
				
				if(location == null)
					return;
				
				Random random = Murder.random;
				ItemMeta meta = item.getItemMeta();
				
				if(meta.getDisplayName() == null)
					meta.setDisplayName("GOLD-0");
				else
					meta.setDisplayName("GOLD-" + (meta.getDisplayName().contains("-") ? (Integer.parseInt(meta.getDisplayName().split("-")[1]) + 1) : 0));
				
				item.setItemMeta(meta);
				
				location.getWorld().dropItem(location, item.clone()).setVelocity(new Vector(0, .5, 0).add(MathUtil.getRandomCircleVector().multiply(random.nextDouble() / 5)));
			}
		};
	}
	
	public Location checkArea(Location location, int CurrentLocation, int fails) {
		Collection<Entity> entityInArea = location.getWorld().getNearbyEntities(location, 4.5, 4.5, 4.5);
		int goldCount = 0;
		
		for(Entity entity : entityInArea)
			if(entity instanceof Item) {
				goldCount++;
				if(goldCount == 2) {
					fails++;
					
					if(fails >= (locations.size() - 1))
						return null;
					
					CurrentLocation++;
					if(CurrentLocation >= locations.size()) {
						CurrentLocation = 1;
					}
					
					location = locations.get(CurrentLocation - 1);
					
					return checkArea(location, CurrentLocation, fails);
				}
			}
		return location;
	}
	
	public void start() {
		if(runnable == null)
			spawn();
		runnable.runTaskTimer(Murder.plugin, 0, 40);
	}
	
	public void stop() {
		runnable.cancel();
		runnable = null;
	}
}