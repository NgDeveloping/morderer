package de.Ng.Murder.Event;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class TeamCalculatedEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	private boolean canceled;
	
	private List<Player> murderer, survivor;
	
	public TeamCalculatedEvent(List<Player> murderer, List<Player> survivor) {
		this.murderer = murderer;
		this.survivor = survivor;
	}
	
	public List<Player> getMurderer() {
		return murderer;
	}
	
	public List<Player> getSurvivor() {
		return survivor;
	}
	
	public boolean isCancelled() {
		return canceled;
	}
	
	public void setMurderer(List<Player> murderer) {
		this.murderer = murderer;
	}
	
	public void setSurvivor(List<Player> surivor) {
		this.survivor = surivor;
	}
	
	public void setChancelled(boolean cancel) {
		this.canceled = cancel;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
}