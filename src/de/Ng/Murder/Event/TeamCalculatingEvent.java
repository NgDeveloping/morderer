package de.Ng.Murder.Event;

import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class TeamCalculatingEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	private boolean canceled;
	
	private Map<Player, int[]> chance;
	
	public TeamCalculatingEvent(Map<Player, int[]> chance) {
		this.chance = chance;
	}
	
	public Map<Player, int[]> getChance() {
		return chance;
	}
	
	public boolean isCancelled() {
		return canceled;
	}
	
	public void setChance(Map<Player, int[]> chance) {
		this.chance = chance;
	}
	
	public void setChancelled(boolean cancel) {
		this.canceled = cancel;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
}