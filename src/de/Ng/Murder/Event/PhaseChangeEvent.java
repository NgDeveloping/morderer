package de.Ng.Murder.Event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import de.Ng.Murder.Phase.PhaseType;

public class PhaseChangeEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	private boolean canceled;
	
	private PhaseType type;
	
	public PhaseChangeEvent(PhaseType type) {
		this.type = type;
	}
	
	public PhaseType getPhaseType() {
		return type;
	}
	
	public boolean isCancelled() {
		return canceled;
	}
	
	public void setPhase(PhaseType type) {
		this.type = type;
	}
	
	public void setChancelled(boolean cancel) {
		this.canceled = cancel;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
}